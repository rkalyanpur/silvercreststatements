﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class Manager
    {
        public static int MaxThreads = Int32.Parse(ConfigurationManager.AppSettings["MaxThreads"]);
        public static List<Thread> threadList = new List<Thread>();
        public static string connStrDW = ConfigurationManager.ConnectionStrings["DWDB"].ConnectionString;
        public static DateTime lastUploadTime = new DateTime(1970, 1, 1, 12, 30, 0, DateTimeKind.Utc);
        //        private SqlConnection connDW;

        public Manager()
        {
  //          connDW = new SqlConnection(connStrDW);

        }

        public void manageBookGeneration()
        {
            Thread th;
            Thread mailingThread= null;
            Thread uploadThread = null;
            int threadCount;

            while (true)
            {
                for (int i = 0; i < threadList.Count; i++)
                {
                    th = threadList[i];
                    //                    if (th.ThreadState != ThreadState.Running)
                    if (!th.IsAlive)
                    {
//                        Console.WriteLine("MAIN THREAD: Removing non Running thread from List - " + th.ThreadState);
                        threadList.Remove(th);
                    }
                }
                threadCount = threadList.Count;
                Console.WriteLine("MAIN THREAD: Number of Active Threads=" + threadCount);

                try
                {
                    //Handling Generation of Client Books AND/OR Statements
                    //Multiple thread can be spawned for this process
                    if (Manager.getNewBookRequests() > threadCount && threadCount < MaxThreads)
                    {
// generateBooks();     //Needs to be commented out if you want to do this in threads.. was only used for debugging non threading       

                        th = new Thread(generateBooks);
                        th.Start();
                        while (!th.IsAlive) ; //Wait for thread to at least start
                        threadList.Add(th);
                        Console.WriteLine("MAIN THREAD: Spawned New Thread =" + threadList.Count);
                    }

                    //Handling Collating of Statements for Mailing/Printing
                    //Only One thread is alive at any moment for these requests
                    if (anyCollateForMailingRequests())
                    {

//                            collateStatements();     //Needs to be commented out if you want to do this in threads.. was only used for debugging non threading       

                        if (mailingThread == null || !mailingThread.IsAlive)
                        {
                            mailingThread = new Thread(collateStatements);
                            mailingThread.Start();
                            while (!mailingThread.IsAlive) ; //Wait for thread to at least start
                            Console.WriteLine("MAIN THREAD: Spawned Thread To Collate Statements By Contact");
                        }

                    }

                    //Handling Request to Upload Statements
                    //Only One thread is alive at any moment for these requests
                    if (anyUploadRequests())
                    {
//                      uploadStatements();     //Needs to be commented out if you want to do this in threads.. was only used for debugging non threading       

                        if (uploadThread == null || !uploadThread.IsAlive)
                        {
                            uploadThread = new Thread(uploadStatements);
                            uploadThread.Start();
                            while (!uploadThread.IsAlive) ; //Wait for thread to at least start
                            Console.WriteLine("MAIN THREAD: Spawned Thread To Upload Statements");
                        }

                    }
                    else
                    {
                        //Only want to move the file if there is no Upload thread that is running, this ensures that it does not try to move a file that is being worked on.
                        if (uploadThread == null || !uploadThread.IsAlive)
                        {
                            StatementUploader.MoveCompletedZipFiles();
                        }
                    }



                }
                catch (Exception ex) { Console.WriteLine("---- ERRROR IN MAIN THREAD --- "); Console.WriteLine(ex.Message); } //Do Nothing just continue

//                Console.WriteLine("MAIN THREAD - Sleeping");
                
                //Requesting statements that are ready to be uploaded to web
                generateUploadQueue();

                //Refreshing Statement Queue so that new Recon Status changes can be incorporated
                refreshStatementQueue();

                Thread.Sleep(20000); //should be 30000 (every 30s )
            }
        }

        private void refreshStatementQueue()
        {
            try
            {
                using (SqlConnection connDW = new SqlConnection(connStrDW))
                using (SqlCommand cmd = new SqlCommand("exec dbo.p_STM_Refresh_Statement_Queue", connDW))
                {
                    connDW.Open();
                    cmd.CommandTimeout = 60;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception x)
            { }

        }

        private void generateUploadQueue()
        {
            try
            {
                using (SqlConnection connDW = new SqlConnection(connStrDW))
                using (SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["SQL_Request_Statement_Upload"], connDW))
                {
                    connDW.Open();
                    cmd.CommandTimeout = 60;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception x)
            { }

        }

        private void generateBooks()
        {
            BookGenerator generator;
            BookRequest req= null;
            BookReport rep;
            ReportParam param;
            string statusSQL = null;
            int collateOrderId;
            int reportId;
            bool moreRequests = true;
            bool successRun = false;
            string fileName = null;
            
            while (moreRequests)
            {
                req = null;
                successRun = false;

                try
                {
                    using (SqlConnection connDW = new SqlConnection(connStrDW))
                    using (SqlCommand cmd = new SqlCommand("exec dbo.p_STM_Get_Next_Request", connDW))
                    {
                        connDW.Open();
                        cmd.CommandTimeout = 60;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            moreRequests = false;
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    req = new BookRequest(
                                         reader.GetInt32(reader.GetOrdinal("book_request_id")),
                                         reader.GetInt32(reader.GetOrdinal("template_id")),
                                        reader.GetString(reader.GetOrdinal("request_type")),
                                        reader.GetString(reader.GetOrdinal("document_type")),
                                        reader.GetBoolean(reader.GetOrdinal("show_page_numbers")),
                                        reader.GetString(reader.GetOrdinal("destination"))
                                    );
                                    moreRequests = true;
                                    Console.WriteLine("Processing request " + req.getBookRequestId());

                                }

                            }

                            reader.NextResult();

                            collateOrderId = -1;
                            rep = null;
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    if (reader.GetInt32(reader.GetOrdinal("collate_order")) != collateOrderId)
                                    {
                                        collateOrderId = reader.GetInt32(reader.GetOrdinal("collate_order"));
                                        reportId = reader.GetInt32(reader.GetOrdinal("report_id"));
                                        rep = new BookReport(reportId, reader["report_code"].ToString(), reader["rptdesign"].ToString(), reader["exclude_condition"].ToString());
                                        req.reportList.Add(rep);
                                    }
                                    param = new ReportParam(reader.GetInt32(reader.GetOrdinal("report_param_id")), reader["report_param_type"].ToString(), reader["report_param_code"].ToString(), reader["report_param_value"].ToString());
                                    rep.paramList.Add(param);
                                }

                                //Excluding Reports if they Pass the Exlcude condition
                                for (int i = req.reportList.Count - 1; i >= 0; i--)
                                {
                                    rep = req.reportList[i];

                                    if (excludeReport(rep))
                                    {
                                        req.reportList.RemoveAt(i);
                                    }

                                }
                                Console.WriteLine("    Got Request Details ..." + req.reportList.Count + " reports");

                            }

                        }
                    }


                    if (moreRequests)
                    {
//                        try
//                        {
                        //Generate Report and save to location
                        generator = new BookGenerator(req);
                        string err = generator.generateBook();
                        if(err == null)
                        {
                            successRun = true;
                            fileName = req.fileName;
                        }

                        //                       }
                        //                       catch (Exception x)
                        else
                        {
                            fileName = req.destination;
                            Console.WriteLine("ERR: " + err);
                            successRun = false;
                        }

                        if (successRun)
                        {
                            statusSQL = "exec dbo.p_STM_Set_Request_Complete 'COMPLETE'," + req.getBookRequestId() + ",'" + fileName + "'";
                        }
                        else
                        {
                            statusSQL = "exec dbo.p_STM_Set_Request_Complete 'ERROR'," + req.getBookRequestId() + ",'" + fileName + "', '" + err + "'";
                        }

                        using (SqlConnection connDW = new SqlConnection(connStrDW))
                        using (SqlCommand dataQry = new SqlCommand(statusSQL, connDW))
                        {
                            connDW.Open();
                            dataQry.CommandTimeout = 60;
                            dataQry.ExecuteNonQuery();
                        }

                    }

                    //Checking if Data Refresh is underway
                    if(Manager.getNewBookRequests() == 0)
                    {
                        moreRequests = false;
                    }

                }
                catch (Exception ex) { Console.WriteLine("---ERROR IN Book Generation Thread ---"); Console.WriteLine(ex.Message); }
            }
            //Thread ends here
            Console.WriteLine("Thread  signing off . . .");

        }

        //Thread to Collate Statements by Contact
        private void collateStatements()
        {
            string asOfDate = null;
            string managerCode = null;
            bool processError = false;

            try
            {
                using (SqlConnection connDW = new SqlConnection(connStrDW))
                using (SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["SQL_NewCollateRequests"], connDW))
                {
                    connDW.Open();
                    cmd.CommandTimeout = 60;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                asOfDate=reader.GetString(reader.GetOrdinal("as_of_date_str"));
                                managerCode = reader.GetString(reader.GetOrdinal("manager_code"));
                            }
                            Console.WriteLine("Received Reqest to collate Statements for " + managerCode + " as of " + asOfDate);
                        }
                    }
                }
            }catch (Exception x) { processError = true;}

            //if process fails, dont throw error just return and try later
            if(processError)
                return;
            
            StatementCollator sc = new StatementCollator();
            try { 
                sc.collateStatements(asOfDate, managerCode);
            }
            catch (Exception x) { processError = true; Console.WriteLine("----ERROR IN COLLATING STATEMENTS ---"); Console.WriteLine(x.Message); }

            //Thread ends here
            Console.WriteLine("--> Collating Thread signing off . . .");

        }

        //Thread to Upload Statements to Web
        private void uploadStatements()
        {
            StatementUploader su = new StatementUploader();
            try
            {
                su.uploadStatements();
                //-------Move Completed Zip ------//
                StatementUploader.MoveCompletedZipFiles();
                lastUploadTime = DateTime.UtcNow;
            }
            catch (Exception x) { Console.WriteLine("----ERROR IN UPLOADING STATEMENTS ---"); Console.WriteLine(x.Message); }

            //Thread ends here
            Console.WriteLine("---> Upload Thread signing off . . .");



        }

        //Get bool value if there are any outstanding requests for Collate to Mail/Print
        public static bool anyCollateForMailingRequests()
        {
            bool newRequests = false;
            try
            {
                using (SqlConnection connDW = new SqlConnection(connStrDW))
                using (SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["SQL_NewCollateRequests"], connDW))
                {
                    connDW.Open();
                    cmd.CommandTimeout = 60;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            newRequests = true;
                        }
                    }
                }
            }
            catch (Exception x) { }
            return newRequests;

        }

        //Get bool value if there are any outstanding requests for Upload to Client Portal
        public static bool anyUploadRequests()
        {
            bool newRequests = false;


              //Only Upload every 
            if (lastUploadTime.AddMinutes(Double.Parse(ConfigurationManager.AppSettings["UPLOAD_FREQUENCY_MINUTES"])) > DateTime.UtcNow) 
                return false;

            try
            {
                //Checking if any files for upload based on if new documents generated
                using (SqlConnection connDW = new SqlConnection(connStrDW))
                using (SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["SQL_New_Upload_Requests"], connDW))
                {
                    connDW.Open();
                    cmd.CommandTimeout = 60;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            newRequests = true;
                        }
                    }
                }


                //Looking if there are any files that were manually dropped off for Upload
                DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(ConfigurationManager.AppSettings["MANUAL_FILE_DROPOFFS"]);
                var filesInDir = hdDirectoryInWhichToSearch.GetFiles("*.pdf").Where(name => !name.FullName.ToUpper().EndsWith("_BAD")); 

                string directoryMANUAL = ConfigurationManager.AppSettings["PORTAL_UPLOAD_FOLDER"] + @"\_WIP\user_pdfs";

                //If there are any files left behind in user_pdfs, then also, true
                DirectoryInfo di;
                if (!Directory.Exists(directoryMANUAL))
                {
                    di = Directory.CreateDirectory(directoryMANUAL);
                }
                di = new DirectoryInfo(directoryMANUAL);
                if(di.GetFiles("*.pdf", SearchOption.TopDirectoryOnly).Length > 0)
                {
                    newRequests = true;
                }

                foreach (FileInfo foundFile in filesInDir)
                {
                    using (SqlConnection connDW = new SqlConnection(connStrDW))
                    using (SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["SQL_Valid_FileName"] + " '" + foundFile.Name + "'", connDW))
                    {
                        connDW.Open();
                        cmd.CommandTimeout = 60;
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    if(reader.GetInt32(reader.GetOrdinal("valid_status"))== 1 ) //Valid File Name
                                    {
                                        newRequests = true;
                                        foundFile.MoveTo(directoryMANUAL + "\\" + foundFile.Name);
                                    }
                                    else
                                    {
                                        if(!File.Exists(foundFile.FullName + "_bad"))
                                        {
                                            foundFile.MoveTo(foundFile.FullName + "_bad");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //Now checking for any Files dropped off by users to be uploaded

            }
            catch (Exception x) { }


            return newRequests;

        }
/*
        public static void ZIPMerge(List<string> files, string dest)
        {
            if (!File.Exists(dest))
            {
                System.IO.Compression.ZipFile.CreateFromDirectory(//.CreateFromDirectory(dirPath, zipFile);
            }

            using (FileStream zipToOpen = new FileStream(dest, FileMode.Open))
            {
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    ZipArchiveEntry readmeEntry = archive.CreateEntry("Readme.txt");
                    using (StreamWriter writer = new StreamWriter(readmeEntry.Open()))
                    {
                        writer.WriteLine("Information about this package.");
                        writer.WriteLine("========================");
                    }
                }
            }

        }
*/

        //PDF Merge is where the source is added to the destination (Appended)
        public static void PDFMerge(List<string> files, string dest, string workingFolder)
        {
            string temp = workingFolder + "\\__tmp.pdf";
            File.Delete(temp);

            using (PdfSharp.Pdf.PdfDocument outPdf = new PdfSharp.Pdf.PdfDocument())
            {
                for (int i = 0; i < files.Count; i++)
                {
                    if (File.Exists(files[i]))
                    {
                        using (PdfSharp.Pdf.PdfDocument fl = PdfSharp.Pdf.IO.PdfReader.Open(files[i], PdfDocumentOpenMode.Import))
                        {
                            CopyPages(fl, outPdf);
                        }
                    }
                }
                outPdf.Save(temp);

            }
            File.Delete(dest);
            File.Move(temp, dest);
        }

        private static void CopyPages(PdfSharp.Pdf.PdfDocument from, PdfSharp.Pdf.PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
            {
                to.AddPage(from.Pages[i]);
            }
        }        


        //Get Number of New Requests in the Database
        public static int getNewBookRequests()
        {
            int count = 0;
            try
            {
                using (SqlConnection connDW = new SqlConnection(connStrDW))
                using (SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["SQL_NewRequests"], connDW))
                {
                    connDW.Open();
                    cmd.CommandTimeout = 60;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            count = reader.GetInt32(reader.GetOrdinal("CT"));
                        }
                    }
                }
            }
            catch (Exception x) { }
            return count;

        }


        private bool excludeReport(BookReport rep)
        {
            bool exclude = false;
            string dbFunction = rep.excludeCondition;

            if (dbFunction == "")
                return exclude;

            StringBuilder sb = new StringBuilder();
            char[] delimiterChars = { '(' };
            sb.Append(dbFunction.Split(delimiterChars)[0]);
            sb.Append("(");

            string functionParams = dbFunction.Split(delimiterChars)[1];
            delimiterChars[0]=')';
            functionParams = functionParams.Split(delimiterChars)[0];

            delimiterChars[0]=',';

            string[] dbFuncParts = functionParams.Split(delimiterChars);
            string newString = null;
            string paramType = null;
            string paramValue = null;


            foreach (string s in dbFuncParts)
            {
                newString = s.Trim();
                paramType = rep.getReportParamType(newString);
                paramValue = rep.getReportParamValue(newString);

                if(paramType != null)
                {
                    if (paramValue != null)
                    {
                        if (paramType.ToUpper().Equals("B") && paramValue.ToUpper().Equals("TRUE"))
                        {
                            newString = "1";
                        }
                        else if (paramType.ToUpper().Equals("B") && paramValue.ToUpper().Equals("FALSE"))
                        {
                            newString = "0";
                        }
                        else
                        {
                            newString = "'" + paramValue + "'";
                        }
                    }                   
                }
                sb.Append(newString+",");
            }

            sb.Remove(sb.Length - 1, 1);

            sb.Append(")");

            try
            {
                using (SqlConnection connDW = new SqlConnection(connStrDW))
                using (SqlCommand cmd = new SqlCommand("SELECT " + sb.ToString(), connDW))
                {
                    connDW.Open();
                    cmd.CommandTimeout = 60;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            if (reader.GetBoolean(0))
                                exclude = true;
                        }
                    }
                }
            }
            catch (Exception x) { }


             return exclude;


        }


    }
}
