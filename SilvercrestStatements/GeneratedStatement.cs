﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class GeneratedStatement
    {
        private int entityId;
        private int isGroup;
        private DateTime asOfDate;
        private int templateId;
        private int deliveryTypeId;
        private string entityCode;
        private string destination;
        private string documentType;

        public GeneratedStatement(int entityId, int isGroup, DateTime asOfDate, int templateId, int deliveryTypeId,string entityCode,string destination, string documentType)
        {
            this.entityId = entityId;
            this.isGroup = isGroup;
            this.asOfDate = asOfDate;
            this.templateId = templateId;
            this.deliveryTypeId = deliveryTypeId;
            this.entityCode = entityCode;
            this.destination = destination;
            this.documentType = documentType;

        }

        public int EntityId
        {
            get
            {
                return entityId;
            }
        }

        public int IsGroup
        {
            get
            {
                return isGroup;
            }
        }

        public DateTime AsOfDate
        {
            get
            {
                return asOfDate;
            }
        }

        public int TemplateId
        {
            get
            {
                return templateId;
            }
        }

        public int DeliveryTypeId
        {
            get
            {
                return deliveryTypeId;
            }
        }

        public string EntityCode
        {
            get
            {
                return entityCode;
            }
        }

        public string Destination
        {
            get
            {
                return destination;
            }
        }

        public string DocumentType
        {
            get
            {
                return documentType;
            }
        }
    }
}
