﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class BookReport
    {
        public int bookReportId;
        public string bookReportCode;
        public string bookReportDesign;
        public string excludeCondition;
        public List<ReportParam> paramList;

        public BookReport(int bookReportId, string bookReportCode, string bookReportDesign, string excludeCondition)
        {
            this.bookReportId = bookReportId;
            this.bookReportCode = bookReportCode;
            this.bookReportDesign = bookReportDesign;
            this.excludeCondition = excludeCondition;
            paramList = new List<ReportParam>();
        }

        public string getReportParamValue(string code)
        {
            string val = null;
            try
            {
                ReportParam param = paramList.Find(item => item.reportParamCode.ToUpper() == code.ToUpper());
                val = param.reportParamValue;
            }
            catch (Exception x) { }
            return val;
        }


        public string getReportParamType(string code)
        {
            string val = null;
            try
            {
                ReportParam param = paramList.Find(item => item.reportParamCode.ToUpper() == code.ToUpper());
                val = param.reportParamType;
            }
            catch (Exception x) { }
            return val;
        }


    }
}
