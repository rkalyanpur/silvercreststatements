﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class ContactStatements
    {
        public int contactId;
        public string contactCode;
        public string displayName;
        public List<string> generatedStatements;
        int numPages;

        public ContactStatements(int contactId, string contactCode, string displayName)
        {
            this.contactCode = contactCode;
            this.contactId = contactId;
            this.displayName = displayName;
            generatedStatements = new List<string>();
            numPages = 0;
        }
    }
}
