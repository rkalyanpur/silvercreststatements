﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace SilvercrestStatements
{
    // ==================================================================================
    /// <summary>
    /// This static class involves helper methods that use strings.
    /// </summary>
    // ==================================================================================
    public static class StringHelper
    {

        public static Dictionary<string, string> HTMLAttributes(string tag)
        {
            Dictionary<string, string> attr = new Dictionary<string, string>();

            MatchCollection matches =
                Regex.Matches(tag, @"([^\t\n\f \/>""'=]+)(?:=)('.*?'|"".*?"")(?:\s|\/>|\>)");

            foreach (Match match in matches)
            {
                attr.Add(match.Groups[1].Value,
                    match.Groups[2].Value.Substring(1, match.Groups[2].Value.Length - 2)
                    );
            }

            return attr;
        }

    }
}
