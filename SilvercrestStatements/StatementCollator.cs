﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class StatementCollator
    {
        private List<ContactStatements> successList;
        private List<ContactStatements> failureList;

        public StatementCollator()
        {
            successList = new List<ContactStatements>();
            failureList = new List<ContactStatements>();
        }

        public void collateStatements(string asOfDate, string managerCode)
        {
            string serverURL1 = ConfigurationManager.AppSettings["ReportServerURL1"];
            string serverURL2 = ConfigurationManager.AppSettings["ReportServerURL2"];
            StringBuilder url;

            ContactStatements contactStatements = null;
            int contactId = 0;
            string contactCode = null;
            string destination = null;
            string displayName = null;

            using (SqlConnection connDW = new SqlConnection(Manager.connStrDW))
            using (SqlCommand cmd = new SqlCommand("exec dbo.p_STM_Statements_Per_Contact '" + asOfDate + "','" + managerCode + "','Mail'", connDW))
            {
                connDW.Open();
                cmd.CommandTimeout = 60;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (reader.GetInt32(reader.GetOrdinal("contact_id")) != contactId)
                            {
                                contactId = reader.GetInt32(reader.GetOrdinal("contact_id"));
                                contactCode = reader.GetString(reader.GetOrdinal("contact_code"));
                                displayName = reader.GetString(reader.GetOrdinal("display_name"));
                                contactStatements = new ContactStatements(contactId, contactCode, displayName);
                                successList.Add(contactStatements);
                            }
                            destination = reader.GetString(reader.GetOrdinal("destination"));
                            contactStatements.generatedStatements.Add(destination);

                        }
                        Console.WriteLine("Got Full List of Contact Statements, now to begin processing");

                    }
                }
            }

            string directoryWIP = ConfigurationManager.AppSettings["STATEMENTS_FOLDER"] + managerCode + "\\" + asOfDate.Substring(0, 6) + "\\_WIP";
            //Checking Directory and fileName
            if (!Directory.Exists(directoryWIP))
            {
                DirectoryInfo di = Directory.CreateDirectory(directoryWIP);
            }

            string directoryPRINT = ConfigurationManager.AppSettings["STATEMENTS_FOLDER"] + managerCode + "\\" + asOfDate.Substring(0, 6) + "\\_PRINTABLES";
            //Checking Directory and fileName
            if (!Directory.Exists(directoryPRINT))
            {
                DirectoryInfo di = Directory.CreateDirectory(directoryPRINT);
            }

            //Getting all files in the folder PRINTABLES
            int lastPrintItem = 0;
            int currentPrintItem = 0;
            DirectoryInfo d = new DirectoryInfo(directoryPRINT);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.pdf"); //Getting Text files
            foreach (FileInfo file in Files)
            {
                if (file.Name.IndexOf('_') > 0)
                {
                    try
                    {
                        currentPrintItem = Int16.Parse(file.Name.Substring(0, file.Name.IndexOf('_')));
                        if (currentPrintItem > lastPrintItem)
                        {
                            lastPrintItem = currentPrintItem;
                        }
                    }
                    catch (Exception xy) { }
                };
            }
            //Setting up fileNames and also
            string fileName = directoryWIP + "\\" + (lastPrintItem + 1).ToString() + "_" + managerCode + "_" + asOfDate.Substring(0, 6) + ".pdf";
            string finalFileName = directoryPRINT + "\\" + (lastPrintItem + 1).ToString() + "_" + managerCode + "_" + asOfDate.Substring(0, 6) + ".pdf";
            string addressPagesWIP = directoryWIP + "\\" + (lastPrintItem + 1).ToString() + "_" + managerCode + "_" + asOfDate.Substring(0, 6) + "_ADDRESS.pdf";
            string addressPagesPRINT = directoryPRINT + "\\" + (lastPrintItem + 1).ToString() + "_" + managerCode + "_" + asOfDate.Substring(0, 6) + "_ADDRESS.pdf";
            string CSV = directoryPRINT + "\\" + (lastPrintItem + 1).ToString() + "_" + managerCode + "_" + asOfDate.Substring(0, 6) + "_CHECKLIST.csv";

            File.Delete(fileName);
            File.Delete(finalFileName);
            File.Delete(addressPagesWIP);
            File.Delete(addressPagesPRINT);
            File.Delete(CSV);

            string tempFileName = directoryWIP + "\\temp.pdf";
            string tempAddress = directoryWIP + "\\tempAddress.pdf";
            bool receivedStream = false;
            int attemptCount = 1;
            string serverURL = null;
            bool contactSuccess = false;
            List<string> pdfs = null;

            //Looping List to create the Printable Book
            CustomRequest customReq = new CustomRequest();
            int idx = 0;

            string clientLetter = ConfigurationManager.AppSettings["CLIENT_LETTER"];
            string disclaimer = ConfigurationManager.AppSettings["DISCLAIMER"];
            int disclaimerMonth = int.Parse(ConfigurationManager.AppSettings["DISCLAIMER_MONTH"].ToString());


            while (idx < successList.Count)
            {
                //Processing Each Contact 
                File.Delete(tempFileName);
                File.Delete(tempAddress);
                contactStatements = successList[idx];
                contactCode = contactStatements.contactCode;
                contactSuccess = false;

                try
                {
                    //Smaller Temp PDFs for Each Contact
                    using (FileStream stream = new FileStream(tempFileName, FileMode.Create))
                    using (Document doc = new Document())
                    using (PdfCopy pdf = new PdfCopy(doc, stream))
                    {
                        doc.Open();

                        PdfReader reader = null;
                        PdfImportedPage page = null;

                        //-------First GENERATING URL TO GET AddressPage -----//
                        url = new StringBuilder();

                        //CREATING URL
                        url.Append(Uri.EscapeUriString(ConfigurationManager.AppSettings["ADDRESS_PAGE"]));
                        url.Append("&ContactCode=" + Uri.EscapeUriString(contactCode));
                        url.Append("&ManagerCode=" + Uri.EscapeUriString(managerCode));
                        url.Append("&__fittopage=true");

                        //Trying three times because some times pdf reader has issues
                        receivedStream = false;
                        attemptCount = 1;
                        serverURL = serverURL1;
                        while (attemptCount <= 4 && !receivedStream)
                        {
                            //if Primary not working, then go to Secondary
                            if (attemptCount > 2)
                            {
                                serverURL = serverURL2;
                            }

                            try
                            {
                                //                            Console.WriteLine(url.ToString());
                                HttpWebRequest request = customReq.GetNewRequest(serverURL + url.ToString(), customReq.cookies);
                                HttpWebResponse response = customReq.MakeRequest(request, customReq.cookies, null);
                                reader = new PdfReader(response.GetResponseStream());
                                receivedStream = true;
                            }
                            catch (Exception ignore)
                            {
                                Console.WriteLine("WRN: Trying Again: " + attemptCount);
                                Console.WriteLine(ignore.Message);
                                Thread.Sleep(2000);

                            }
                            attemptCount++;
                        }
                        if (!receivedStream)
                        {
                            throw new Exception();
                        }

                        for (int k = 0; k < reader.NumberOfPages; k++)
                        {
                            page = pdf.GetImportedPage(reader, k + 1);
                            pdf.AddPage(page);

                            //Address Page Logic
                            using (FileStream streamAddress = new FileStream(tempAddress, FileMode.Create))
                            using (Document docAddress = new Document())
                            using (PdfCopy pdfAddress = new PdfCopy(docAddress, streamAddress))
                            {
                                docAddress.Open();

                                pdfAddress.AddPage(page);
                            }
                        }

                        pdf.FreeReader(reader);
                        reader.Close();

                        //Attaching Client Letter if it exists//
                        if (File.Exists(clientLetter))
                        {
                            reader = new PdfReader(clientLetter);
                            for (int k = 0; k < reader.NumberOfPages; k++)
                            {
                                page = pdf.GetImportedPage(reader, k + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();
                        }

                        //-------Second Attaching all statements to main pdf -----//
                        for (int j = 0; j < contactStatements.generatedStatements.Count; j++)
                        {
                            reader = new PdfReader(contactStatements.generatedStatements[j]);
                            for (int k = 0; k < reader.NumberOfPages; k++)
                            {
                                page = pdf.GetImportedPage(reader, k + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();
                        }


                        //Adding Disclaimer if it is the Month 12
                        if (DateTime.ParseExact(asOfDate, "yyyyMMdd", CultureInfo.InvariantCulture).Month == disclaimerMonth)
                        {
                            reader = new PdfReader(disclaimer);
                            for (int k = 0; k < reader.NumberOfPages; k++)
                            {
                                page = pdf.GetImportedPage(reader, k + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();
                        }

                    }

                    idx++;
                    contactSuccess = true;
                }
                catch (Exception x)
                {
                    failureList.Add(contactStatements);
                    successList.Remove(contactStatements);
                    //throw x;
                }

                //Adding temp pdf to Master Pdf all in _WIP folder

                if (contactSuccess)
                {
                    pdfs = new List<string>();
                    pdfs.Add(fileName);
                    pdfs.Add(tempFileName);
                    Manager.PDFMerge(pdfs, fileName, directoryWIP);

                    //Address Pages
                    pdfs = new List<string>();
                    pdfs.Add(addressPagesWIP);
                    pdfs.Add(tempAddress);
                    Manager.PDFMerge(pdfs, addressPagesWIP, directoryWIP);

                }

            }

            File.Move(fileName, finalFileName);
            File.Move(addressPagesWIP, addressPagesPRINT);
            //Creating the CSV file
            StringBuilder sb = new StringBuilder();
            string str = null;
            int iTmp = -1;
            for (int i = 0; i < successList.Count; i++)
            {
                contactStatements = successList[i];
                for (int j = 0; j < contactStatements.generatedStatements.Count; j++)
                {
                    str = contactStatements.generatedStatements[j];
                    iTmp = str.LastIndexOf('\\') + 1;
                    str = str.Substring(iTmp, str.Length - iTmp - 4);
                    sb.Append(contactStatements.displayName + "," + str.Substring(0, str.IndexOf('_')) + "," + contactStatements.generatedStatements[j]);
                    sb.Append(Environment.NewLine);
                }


            }
            System.IO.File.WriteAllText(CSV, sb.ToString());
            //Reusing variable
            contactSuccess = false;
            string statusSQL = null;
            //Updating Database with Success/ERROR details
            using (SqlConnection connDW = new SqlConnection(Manager.connStrDW))
            {
                connDW.Open();

                SqlTransaction sqlTran = connDW.BeginTransaction();

                SqlCommand command = connDW.CreateCommand();
                command.Transaction = sqlTran;

                try
                {
                    for (int i = 0; i < successList.Count; i++)
                    {
                        contactStatements = successList[i];
                        command.CommandText = "exec p_STM_Update_Statement_Delivery_Queue '" + asOfDate + "','" + contactStatements.contactCode + "','" + @managerCode + "','" + finalFileName + "'";
                        command.ExecuteNonQuery();
                    }

                    for (int i = 0; i < failureList.Count; i++)
                    {
                        contactStatements = failureList[i];
                        command.CommandText = "exec p_STM_Update_Statement_Delivery_Queue '" + asOfDate + "','" + contactStatements.contactCode + "','" + @managerCode + "','ERROR'";
                        command.ExecuteNonQuery();
                    }

                    // your code
                    sqlTran.Commit();
                    contactSuccess = true;
                }
                catch (Exception xx)
                {
                    Console.WriteLine("-------Error While Looping Each Contact to Update Statement Delivery------");
                    Console.WriteLine(xx.Message);
                    try
                    {
                        sqlTran.Rollback();
                        contactSuccess = false;
                        File.Delete(finalFileName);
                        File.Delete(addressPagesPRINT);
                        File.Delete(CSV);
                    }
                    catch (Exception yy)
                    { Console.WriteLine("-------Error While Rolling Back from Update Statement Delivery------"); Console.WriteLine(yy.Message); }

                }

            }


        }

/*
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
*/
  }
}
