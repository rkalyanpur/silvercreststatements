﻿
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SilvercrestStatements
{
    class BookGenerator
    {
        private BookRequest bookRequest;
        private static int MAX_ATTEMPTS = 2;

        public BookGenerator(BookRequest bookRequest)
        {
            this.bookRequest = bookRequest;
        }

        public string generateBook()
        {
            int currentPage = 1;
            string serverURL1 = ConfigurationManager.AppSettings["ReportServerURL1"];
            string serverURL2 = ConfigurationManager.AppSettings["ReportServerURL2"];
            StringBuilder url;
            ReportParam param;
            BookReport rep;
            int i, j;
            CustomRequest customReq = new CustomRequest();
            string errMsg = null;

            //Checking Directory and fileName
            if (!Directory.Exists(bookRequest.destination))
            {
                DirectoryInfo di = Directory.CreateDirectory(bookRequest.destination);
            }

            string fileName = bookRequest.destination + "\\" + bookRequest.getFileName() + ".pdf";

            //*****Setting the fileName for This Request****
            bookRequest.fileName = fileName;


            //CALLING SERVER FOR REPORT
            //Override PageNumber start for hiding of page numbers
            if (!bookRequest.showPageNumbers)
            {
                currentPage = -1000000;
            }

            int count = 1;

            string fileNameOnly = Path.GetFileNameWithoutExtension(fileName);
            string extension = Path.GetExtension(fileName);
            string path = Path.GetDirectoryName(fileName);
            string newFullPath = fileName;

            string errMessage = "Unknown Error during Report Generation";


            System.IO.DirectoryInfo dinfo = new DirectoryInfo(path);

            //For statements only, not custom books, you delete teh old ones with Same Document Type and Template Id.
          //Cannot Delete because there may be Tax Packs or multiple statements for a given account
           if (path.StartsWith("S:"))
            {
                foreach (FileInfo file in dinfo.GetFiles())
                {
                    if (file.Name.ToUpper().Contains(bookRequest.documentType.Replace(" ", "").ToUpper() + "_" + bookRequest.templateId + "_"))
                    {
                        file.Delete();
                    }
                }
            }

 
            while (File.Exists(newFullPath))
            {
                string tempFileName = string.Format("{0}_{1}", fileNameOnly, count++);
                newFullPath = Path.Combine(path, tempFileName + extension);
            }
            if (!newFullPath.Equals(fileName))
            {
                File.Move(fileName, newFullPath);
            }

            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                using (Document doc = new Document())
                using (PdfCopy pdf = new PdfCopy(doc, stream))
                {
                    doc.Open();

                    PdfReader reader = null;
                    PdfImportedPage page = null;


                    for (i = 0; i < bookRequest.reportList.Count; i++)
                    {
                        url = new StringBuilder();
                        //                            url = new StringBuilder(serverURL); //Primary Actuate Server
                        rep = bookRequest.reportList[i];

                        if (rep.bookReportCode.ToUpper().Equals("ATTCH"))
                        {
                            if (rep.paramList[0].reportParamValue.ToUpper().EndsWith(".PDF"))
                            {
                                reader = new PdfReader(rep.paramList[0].reportParamValue);
                            }
                            else
                            {
                                reader = new PdfReader(rep.paramList[0].reportParamValue + ".pdf");
                            }
                        }
                        else
                        {
                            //CREATING URL
                            url.Append(Uri.EscapeUriString(rep.bookReportDesign));
                            for (j = 0; j < rep.paramList.Count; j++)
                            {
                                param = rep.paramList[j];
                                url.Append("&" + param.reportParamCode + "=" + Uri.EscapeUriString(param.reportParamValue));
                            }
                            //PageNumbers
                            url.Append("&" + "PAGENUMBER=" + currentPage.ToString());


                            //Trying three times because some times pdf reader has issues
                            bool receivedStream = false;
                            int attemptCount = 1;
                            string serverURL = serverURL1;
                            
                            while (attemptCount <= MAX_ATTEMPTS && !receivedStream)
                            {

                                try
                                {
                                    //                            Console.WriteLine(url.ToString());
                                    HttpWebRequest request = customReq.GetNewRequest(serverURL + url.ToString(), customReq.cookies);
//                                                                request.AllowAutoRedirect = true;


                                    HttpWebResponse response = customReq.MakeRequest(request, customReq.cookies, null);
                                    Uri baseUrl = new Uri(request.Address.AbsoluteUri);
                                    if (!response.ContentType.Contains("pdf"))
                                    {
                                        Console.WriteLine("Non Pdf content");
                                        /*
                                                                                string strTemp = null;
                                                                                Thread.Sleep(1000);
                                                                                Stream responseStream = responseStream = response.GetResponseStream();
                                                                                if (response.ContentEncoding.ToLower().Contains("gzip"))
                                                                                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                                                                                else if (response.ContentEncoding.ToLower().Contains("deflate"))
                                                                                    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);

                                                                                StreamReader Reader = new StreamReader(responseStream, Encoding.Default, true);

                                                                                string Html = Reader.ReadToEnd();
                                                                                //                                        Html = StringHelper.StringFromVerbatimLiteral(Html);
                                                                                Html = Html.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace("&#32;", " ").Trim();
                                                                                Html = Html.Substring(Html.ToUpper().LastIndexOf("<FORM"));

                                                                                strTemp = Html.Substring(0, Html.IndexOf(">")+1);
                                                                                strTemp = StringHelper.HTMLAttributes(strTemp)["action"];
                                                                                Uri postUrl = new Uri(baseUrl, strTemp);
                                                                                Dictionary<string, string> formElements = new Dictionary<string, string>();
                                                                                strTemp = Html.Substring(Html.IndexOf(">") + 1);
                                                                                Dictionary<string, string> dTemp = null;
                                                                                int startOfTag, endOfTag = -1;
                                                                                startOfTag = strTemp.ToUpper().IndexOf("<INPUT");
                                                                                while (startOfTag >= 0)
                                                                                {
                                                                                    endOfTag = strTemp.IndexOf(">") + 1;
                                                                                    dTemp = StringHelper.HTMLAttributes(strTemp.Substring(startOfTag, endOfTag+1));
                                                                                    formElements.Add(dTemp["name"], HttpUtility.UrlDecode(dTemp["value"]));
                                                                                    strTemp = strTemp.Substring(endOfTag);
                                                                                    startOfTag = strTemp.ToUpper().IndexOf("<INPUT");
                                                                                }


                                                                                responseStream.Close();
                                                                                response.Close();

                                                                                request = customReq.GetNewRequest(postUrl.ToString(), customReq.cookies);
                                                                                request.Timeout = 180000;  //3 minutes - shown in milliseconds
                                                                                //When a report takes longer than usual, Actuate Server sends back another HTML file in a zip format that needs to be reposted to get the PDF
                                                                                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");


                                                                                response = customReq.MakeRequest(request, customReq.cookies, formElements);

                                                                                responseStream = responseStream = response.GetResponseStream();
                                                                                Reader = new StreamReader(responseStream, Encoding.Default, true);

                                                                                Html = Reader.ReadToEnd();
                                                                                Html = Html.Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
                                        */
                                    }

                                    reader = new PdfReader(response.GetResponseStream());
                                    receivedStream = true;
                                    response.Close();                            
                                }
                                catch (Exception ignore)
                                {
                                    Console.WriteLine("WRN: Trying Again: " + attemptCount);
                                    Console.WriteLine(ignore.Message);
                                    Thread.Sleep(5000);

                                }
                                attemptCount++;
                            }


                            if (!receivedStream)
                            {
                                errMessage =  url.ToString();
                                throw new Exception();
                            }

                        }


                        //Except for If Cover page, all others we would count Number of Pages
                        if (!rep.bookReportCode.ToUpper().Equals("CVR"))
                        {
                            currentPage = currentPage + reader.NumberOfPages;
                        }

                        for (int k = 0; k < reader.NumberOfPages; k++)
                        {
                            page = pdf.GetImportedPage(reader, k + 1);
                            pdf.AddPage(page);
                        }

                        pdf.FreeReader(reader);
                        reader.Close();
                    }
                }
            }
            catch (Exception x)
            {
                File.Delete(fileName);
                return errMessage;
//                throw x;
            }

            return null;


        }


        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
    }
}
