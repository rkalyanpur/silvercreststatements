﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class StatementUploader
    {
        private List<GeneratedStatement> successList;
        private List<GeneratedStatement> failureList;

        public StatementUploader()
        {
            successList = new List<GeneratedStatement>();
            failureList = new List<GeneratedStatement>();
        }

        //Unfortunately Code Repeated ---to force file moves even if no new files for upload ---//
        public static void MoveCompletedZipFiles()
        {
            string directoryUPLOAD = ConfigurationManager.AppSettings["PORTAL_UPLOAD_FOLDER"];
            string directoryWIP = directoryUPLOAD + "\\_WIP";


            //Checking Directory and fileName
            /*            DirectoryInfo di;
                        if (!Directory.Exists(directoryWIP + "\\pdfs"))
                        {
                            di = Directory.CreateDirectory(directoryWIP + "\\pdfs");
                        }

                        string directoryUPLOAD = null;

                        string today = DateTime.Now.ToString("MMMdd");
                        string time = "2359";
                        int zipEntries = 0;
                        bool moveZip = false;
                        string newFileName = null;
            */
            DirectoryInfo di = new DirectoryInfo(directoryWIP);
            //Moving Completed OR Full (Over 500 Entries) Zip files to Upload Folder
            foreach (FileInfo file in di.GetFiles("*.zip", SearchOption.TopDirectoryOnly))
            {
                if (!file.Name.ToUpper().StartsWith("BACKUP"))
                {
                    /*
                                        moveZip = false;

                                        using (ZipArchive archive = ZipFile.Open(file.FullName, ZipArchiveMode.Read))
                                        {
                                            zipEntries = archive.Entries.Count;
                                            if (zipEntries > 500 || (!file.Name.ToUpper().StartsWith(today.ToUpper() + "_")))
                                            {
                                                moveZip = true;

                                                //if it is today's file that has over 500 entries, then put in hour and minute otherwise will default to 2359
                                                if (file.Name.ToUpper().StartsWith(today.ToUpper() + "_"))
                                                {
                                                    time = DateTime.Now.ToString("HHmm");
                                                }

                                                foreach (ZipArchiveEntry entry in archive.Entries)
                                                {
                                                    if (entry.Name.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        string str = entry.Name;
                                                        str = str.Substring(str.IndexOf("~^") + 2, 7);
                                                        DateTime asOfDate = DateTime.ParseExact(str, "MMMyyyy", CultureInfo.InvariantCulture);

                                                        directoryUPLOAD = ConfigurationManager.AppSettings["UPLOAD_FOLDER"] + "\\" + asOfDate.ToString("yyyyMM");
                                                        //Checking Directory and fileName
                                                        if (!Directory.Exists(directoryUPLOAD + "\\_UPLOADED"))
                                                        {
                                                            di = Directory.CreateDirectory(directoryUPLOAD + "\\_UPLOADED");
                                                        }

                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        //Move zip file with any other date
                                        if (moveZip)
                                        {
                                            newFileName = file.Name.Substring(file.Name.LastIndexOf('\\') + 1);
                                            newFileName = newFileName.Insert(newFileName.Length - 4, "_" + time);
                                            file.MoveTo(directoryUPLOAD + "\\" + newFileName);
                                        }
                    */
                    file.MoveTo(directoryUPLOAD + "\\" + file.Name.Substring(file.Name.LastIndexOf('\\') + 1));
                }

            }

        }
        
        public void uploadStatements()
        {
            GeneratedStatement generatedStatement = null;
            int entityId = 0;
            int isGroup;
            DateTime asOfDate=new DateTime();
            int templateId;
            int deliveryTypeId;
            string entityCode;
            string destination;
            string documentType;

            using (SqlConnection connDW = new SqlConnection(Manager.connStrDW))
            using (SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["SQL_New_Upload_Requests"], connDW))
            {
                connDW.Open();
                cmd.CommandTimeout = 60;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            entityId = reader.GetInt32(reader.GetOrdinal("entity_id"));
                            isGroup = Convert.ToInt32(reader.GetBoolean(reader.GetOrdinal("is_group")));
                            templateId = reader.GetInt32(reader.GetOrdinal("template_id"));
                            deliveryTypeId = reader.GetInt32(reader.GetOrdinal("delivery_type_id"));
                            DateTime.TryParse(reader["as_of_date"].ToString(), out asOfDate);
                            entityCode = reader["entity_code"].ToString();
                            destination = reader["statement_destination"].ToString();
                            documentType = reader["document_type"].ToString();
                            
                            generatedStatement = new GeneratedStatement(entityId, isGroup, asOfDate, templateId, deliveryTypeId, entityCode, destination, documentType);
                            successList.Add(generatedStatement);

                        }
                        Console.WriteLine("Got Full List of Statements For Upload, now to begin processing");

                    }
                }
            }

            string directoryUPLOAD = ConfigurationManager.AppSettings["PORTAL_UPLOAD_FOLDER"];
            string directoryWIP = directoryUPLOAD + "\\_WIP";
            //Checking Directory and fileName
            DirectoryInfo di;
            if (!Directory.Exists(directoryWIP + "\\pdfs"))
            {
                di = Directory.CreateDirectory(directoryWIP+"\\pdfs");
            }

            //Checking Directory and fileName
            if (!Directory.Exists(directoryUPLOAD + "\\Archive"))
            {
                di = Directory.CreateDirectory(directoryUPLOAD + "\\Archive");
            }

            //Clearing WIP folder of all irrelevant pdfs
            di = new DirectoryInfo(directoryWIP + "\\pdfs");
            foreach (FileInfo file in di.GetFiles("*.pdf", SearchOption.TopDirectoryOnly))
            {
                   file.Delete();
            }


            //-------------------------------------      

            //Name of Zip file
            string timestamp = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string finalFileName = directoryWIP + "\\" + timestamp + "_WebUpload.zip";
            string backupFinalFileName = directoryWIP + "\\BACKUP_" + timestamp + "_WebUpload.zip";

            //If it exists, we first extract all contents to the pdf folder
            if (File.Exists(finalFileName))
            {
                di = new DirectoryInfo(directoryWIP + "\\pdfs");

                ZipFile.ExtractToDirectory(finalFileName, di.FullName);
            }

  //----------------------------------------------          

            //Now creating in right format all statements for upload (overwriting any newones)
            string tempFileName = null;
            bool statementSuccess = false;

            int idx = 0;

            string clientLetter = ConfigurationManager.AppSettings["CLIENT_LETTER"];
            string disclaimer = ConfigurationManager.AppSettings["DISCLAIMER"];
            int disclaimerMonth = int.Parse(ConfigurationManager.AppSettings["DISCLAIMER_MONTH"].ToString());

            FileInfo fio;

            while (idx < successList.Count)
            {
                generatedStatement = successList[idx];

                //Verifying File Size before we copy over//
                fio = new FileInfo(generatedStatement.Destination);
                if (fio.Length == 0)
                {
                    failureList.Add(generatedStatement);
                    successList.Remove(generatedStatement);
                }
                else
                {
                    //                tempFileName = directoryWIP + "\\pdfs\\" + generatedStatement.EntityCode + "~^" + generatedStatement.AsOfDate.ToString("MMMyyyy") + "_" + generatedStatement.DocumentType + "~^.pdf";
                    tempFileName = directoryWIP + "\\pdfs\\" + generatedStatement.Destination.Substring(generatedStatement.Destination.LastIndexOf("\\") + 1);
                    File.Delete(tempFileName);
                    statementSuccess = false;

                    try
                    {
                        //Smaller Temp PDFs for Each Contact
                        using (FileStream stream = new FileStream(tempFileName, FileMode.Create))
                        using (Document doc = new Document())
                        using (PdfCopy pdf = new PdfCopy(doc, stream))
                        {
                            doc.Open();

                            PdfReader reader = null;
                            PdfImportedPage page = null;

                            //Attaching Client Letter if it exists//
                            if (File.Exists(clientLetter))
                            {
                                reader = new PdfReader(clientLetter);
                                for (int k = 0; k < reader.NumberOfPages; k++)
                                {
                                    page = pdf.GetImportedPage(reader, k + 1);
                                    pdf.AddPage(page);
                                }
                                pdf.FreeReader(reader);
                                reader.Close();
                            }

                            //-------Second Attaching Statement -----//
                            reader = new PdfReader(generatedStatement.Destination);
                            for (int k = 0; k < reader.NumberOfPages; k++)
                            {
                                page = pdf.GetImportedPage(reader, k + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();


                            //Adding Disclaimer if it is the Month 12
                            if (generatedStatement.AsOfDate.Month == disclaimerMonth)
                            {
                                reader = new PdfReader(disclaimer);
                                for (int k = 0; k < reader.NumberOfPages; k++)
                                {
                                    page = pdf.GetImportedPage(reader, k + 1);
                                    pdf.AddPage(page);
                                }
                                pdf.FreeReader(reader);
                                reader.Close();
                            }

                        }

                        idx++;
                        statementSuccess = true;

                    }
                    catch (Exception x)
                    {
                        failureList.Add(generatedStatement);
                        successList.Remove(generatedStatement);
                        //throw x;
                    }

                }
            }

            //Copying over the Manual PDFs that Users have dropped off for upload
            DirectoryInfo directoryMANUAL = new DirectoryInfo(directoryWIP + "\\user_pdfs");
            foreach (var file in directoryMANUAL.GetFiles("*.pdf", SearchOption.TopDirectoryOnly))
            {
                File.Copy(file.FullName, Path.Combine(directoryWIP + "\\pdfs", file.Name));                                   
            }


            //Dealing with ZIP files
            if (File.Exists(finalFileName))
            {
                File.Move(finalFileName, backupFinalFileName);
            }

            //FINALLY REZIPPING ALL CONTENTS OF PDF FOLDER (unzipped + new)
            try
            {
                System.IO.Compression.ZipFile.CreateFromDirectory(directoryWIP + "\\pdfs", finalFileName, CompressionLevel.Optimal, false);
            }
            catch (Exception)
            {
                //Exception during zipping so we first remove the partial zip if one was created
                if (File.Exists(finalFileName))
                {
                    File.Delete(finalFileName);
                }

                //If zipping was a success we clean up the backup
                if (File.Exists(backupFinalFileName))
                {
                    File.Move(backupFinalFileName, finalFileName);
                }
            }
            finally
            {
                //If zipping was a success we clean up the backup
                if (File.Exists(backupFinalFileName))
                {
                    File.Delete(backupFinalFileName);
                }
            }

            di = new DirectoryInfo(directoryWIP + "\\pdfs");
            foreach (FileInfo fi in di.GetFiles("*.pdf", SearchOption.TopDirectoryOnly))
            {
                fi.Delete();
            }

            //            File.Move(tempZip, finalFileName);
            statementSuccess = false;
            //Updating Database with Success/ERROR details
            using (SqlConnection connDW = new SqlConnection(Manager.connStrDW))
            {
                connDW.Open();

                SqlTransaction sqlTran = connDW.BeginTransaction();

                SqlCommand command = connDW.CreateCommand();
                command.Transaction = sqlTran;

                try
                {
                    for (int i = 0; i < successList.Count; i++)
                    {
                        generatedStatement = successList[i];
                        command.CommandText = "exec dbo.p_STM_Update_Statement_Upload_Queue '" + generatedStatement.AsOfDate.ToString("MM/dd/yyyy") + "','" + generatedStatement.EntityCode + "'," + generatedStatement.TemplateId + ",'Web','" + finalFileName.Substring(finalFileName.LastIndexOf("\\")+1) + "'";
                        command.ExecuteNonQuery();
                    }

                    for (int i = 0; i < failureList.Count; i++)
                    {
                        generatedStatement = failureList[i];
                        command.CommandText = "exec dbo.p_STM_Update_Statement_Upload_Queue '" + generatedStatement.AsOfDate.ToString("MM/dd/yyyy") + "','" + generatedStatement.EntityCode + "'," + generatedStatement.TemplateId + ",'Web','ERROR'";
                        command.ExecuteNonQuery();
                    }

                    // your code
                    sqlTran.Commit();
                    statementSuccess = true;

                    //Deleting Manual PDF files only after the Database Update was also successful
                    foreach (FileInfo fi in directoryMANUAL.GetFiles("*.pdf", SearchOption.TopDirectoryOnly))
                    {
                        fi.Delete();
                    }



                }
                catch (Exception xx)
                {
                    Console.WriteLine("-------Error While Looping Each Statement to Update Upload Status------");
                    Console.WriteLine(xx.Message);
                    try
                    {
                        sqlTran.Rollback();
                        statementSuccess = false;
                        //                        File.Delete(finalFileName);
                    }
                    catch (Exception yy)
                    { Console.WriteLine("-------Error While Rolling Back from Update Upload Status-----"); Console.WriteLine(yy.Message); }

                }

            }


        }

        /*
                public static void CopyStream(Stream input, Stream output)
                {
                    byte[] buffer = new byte[8 * 1024];
                    int len;
                    while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        output.Write(buffer, 0, len);
                    }
                }
         */
    }
}
