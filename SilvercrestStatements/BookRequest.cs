﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class BookRequest
    {
        public int bookRequestId;
        public int templateId;
        public string requestType;
        public string documentType;
        public bool showPageNumbers;
        public string destination;
        public string fileName;
        public List<BookReport> reportList;

        public BookRequest(int bookRequestId, int templateId, string requestType, string documentType, bool showPageNumbers, string destination)
        {
            this.bookRequestId = bookRequestId;
            this.templateId = templateId;
            this.requestType = requestType;
            this.documentType = documentType;
            this.showPageNumbers = showPageNumbers;
            this.destination = destination;
            reportList = new List<BookReport>();
        }

        public long getBookRequestId()
        {
            return bookRequestId;
        }

        public string getFileName()
        {
            string portfolioCode = null;
            string asOfDate = null;
            int i = 0;
            while((portfolioCode == null) && (i < reportList.Count))
            {
                portfolioCode = reportList[i].getReportParamValue("Code");
                i++;
            }

            if(portfolioCode == null)
            {
                portfolioCode = "REQUEST";
            }


            i = 0;
            while((asOfDate == null) && (i < reportList.Count))
            {
                asOfDate = reportList[i].getReportParamValue("AsOfDate");
                i++;
            }
            if(asOfDate == null)
            {
                asOfDate = DateTime.Now.ToString("M/d/yyyy");
            }

            string informational = "";
            if(/*(!documentType.ToUpper().StartsWith("CLIENT"))*/ destination.ToUpper().StartsWith("S:"))   //IF condition changed on 1/10/17 so that ClientBook will be coded as such if it is automated, so in S drive
            {
                informational = documentType.Replace(" ", "") +"_" + templateId.ToString() + "_";
            }
            else if(!documentType.ToUpper().Contains("UPLOAD"))
            {
                informational = documentType.Replace(" ", "") + "_" + templateId.ToString() + "_";
            }

            return portfolioCode + "_" + DateTime.ParseExact(asOfDate, "M/d/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd") + "_" + informational + DateTime.Now.ToString("yyyyMMdd-HHmmss");  

        }

    }
}
