﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilvercrestStatements
{
    class ReportParam
    {
        public int reportParamId;
        public string reportParamType;
        public string reportParamCode;
        public string reportParamValue;
        
        public ReportParam(int reportParamId, string reportParamType, string reportParamCode, string reportParamValue)
        {
            this.reportParamCode = reportParamCode;
            this.reportParamId = reportParamId;
            this.reportParamType = reportParamType;
            this.reportParamValue = reportParamValue;
        }
    }
}
