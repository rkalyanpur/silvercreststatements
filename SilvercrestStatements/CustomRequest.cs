﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Threading;

namespace SilvercrestStatements
{
    class CustomRequest
    {
        private Uri absoluteUrl = null;

        //Main 2 so that there are not multiple entry points
        static void Main2(string[] args)
        {
            CustomRequest customReq = new CustomRequest();
            HttpWebRequest request = customReq.GetNewRequest("http://www.elitepvpers.com/forum/login.php?do=login", customReq.cookies);


            string id = "efeerk";
            string pw = "trgamer";
            Dictionary<string, string> parameters = new Dictionary<string, string>{{"vb_login_username",id},{"vb_login_password",pw},
                {"cookieuser","1"},{"s",""},{"securitytoken","1342962102-8aa76183509ebade0188ac49c3c84470ff2aabba"},
                {"do","login"},{"vb_login_md5password",""},{"vb_login_md5password_utf",""}};


            HttpWebResponse response = customReq.MakeRequest(request, customReq.cookies, parameters);
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                if (!reader.EndOfStream)
                {
                    Console.Write(reader.ReadToEnd());
                }
            }

            Console.Read();

        }

        public CookieContainer cookies = new CookieContainer();

        public HttpWebRequest GetNewRequest(string targetUrlInput, CookieContainer SessionCookieContainer)
        {
            if (this.absoluteUrl == null)
            {
                this.absoluteUrl = new Uri(targetUrlInput);
            }
            Uri uri = new Uri(this.absoluteUrl, targetUrlInput);
            string targetUrl = uri.ToString();
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUrl);
            request.CookieContainer = SessionCookieContainer;
            request.Timeout = 600000;  //10 minutes - shown in milliseconds
            //When a report takes longer than usual, Actuate Server sends back another HTML file in a zip format that needs to be reposted to get the PDF
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            request.AllowAutoRedirect = false;
            return request;
        }

        public HttpWebResponse MakeRequest(HttpWebRequest request, CookieContainer SessionCookieContainer, Dictionary<string, string> parameters = null)
        {


            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.CookieContainer = SessionCookieContainer;
            request.AllowAutoRedirect = false;
            

            if (parameters != null)
            {
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                string postData = "";
                foreach (KeyValuePair<String, String> parametro in parameters)
                {
                    if (postData.Length == 0)
                    {
                        postData += String.Format("{0}={1}", parametro.Key, parametro.Value);
                    }
                    else
                    {
                        postData += String.Format("&{0}={1}", parametro.Key, parametro.Value);
                    }

                }
                byte[] postBuffer = UTF8Encoding.UTF8.GetBytes(postData);
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(postBuffer, 0, postBuffer.Length);
                }
            }
            else
            {
                request.Method = "GET";
            }



            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            SessionCookieContainer.Add(response.Cookies);


            while (response.StatusCode == HttpStatusCode.Found)
            {
                response.Close();
                request = GetNewRequest(response.Headers["Location"], SessionCookieContainer);
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
                response = (HttpWebResponse)request.GetResponse();
                SessionCookieContainer.Add(response.Cookies);
            }


            return response;
        }

    }
}


