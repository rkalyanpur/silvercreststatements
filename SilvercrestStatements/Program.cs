﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;   //GuidAttribute
using System.Reflection;                //Assembly
using System.Threading;                 //Mutex
using System.Security.AccessControl;    //MutexAccessRule
using System.Security.Principal;
using System.Diagnostics;        //SecurityIdentifier

namespace SilvercrestStatements
{
    class Program
    {
        static void Main(string[] args)
        {


            //OPTION 1
            string runningProcess = Process.GetCurrentProcess().ProcessName;
            Process[] processes = Process.GetProcessesByName(runningProcess);
//            var exists = Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1;
            if(processes.Length > 1)
            {
                Console.WriteLine("Exiting with Option 1 where same process name found");
                Thread.Sleep(5000);

               Environment.Exit(0); // This will kill my instance.
            }

            if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1)
            {
                Console.WriteLine("Exiting with Option 2 where same process found");
                Thread.Sleep(5000);

                Environment.Exit(0); // This will kill my instance.
            }

            // Perform your work here.
            Manager mgr = new Manager();
            mgr.manageBookGeneration();
           
        }
    }
}
